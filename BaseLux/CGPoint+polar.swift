//
//  CGPoint+polar.swift
//  TeleLux
//
//  Created by Eric Betts on 7/11/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import GLKit

extension CGPoint {
    var r : CGFloat {
        return hypot(x, y)
    }
    
    var ϕ : CGFloat {
        return ((atan2(y, x) * 180.0/CGFloat(M_PI)) + 360) % 360
    }
}