//
//  ViewController.swift
//  BaseLux
//
//  Created by Eric Betts on 7/22/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    @IBOutlet weak var statusLabel : NSTextField!

    let ble = BLE()

    override func viewDidLoad() {
        super.viewDidLoad()
        ble.delegate = self

    }

    override var representedObject: AnyObject? {
        didSet {

        }
    }
    
    @IBAction func colorSelected(sender: AnyObject?) {
        let well = sender as! NSColorWell
        let (_, rgbTuple, _) = well.color.components()!
        let command = NSData(bytes: ["A".asciiValue, UInt8(rgbTuple.red), UInt8(rgbTuple.green), UInt8(rgbTuple.blue)] as [UInt8], length: 4)
        ble.write(data: command)
    }

}

extension ViewController : BLEDelegate {
    func bleDidUpdateState() {
        ble.startScanning(60.0)
        statusLabel.stringValue = "Scanning"
    }
    
    func bleDidConnectToPeripheral() {
        statusLabel.stringValue = "Connected"
    }
    
    func bleDidDisconenctFromPeripheral() {
        statusLabel.stringValue = "Disconnected"
        NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(self.bleDidUpdateState), userInfo: nil, repeats: false)
    }
    
    func bleDidReceiveData(data: NSData?) {
        print("BLE Received: \(data)")
    }
}
